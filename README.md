# Personium

A few weeks ago I wanted to see how I could make a profile picture of myself a bit more interesting. After giving it a shot, trying to do this with HTML/css/javascript induces a spicy headache. Fast forwards three weeks later and turns out just generating a GIF is slightly easier.

![Benzene](atom.gif "Benzene with Bismuth orbitals")

*Generated with:*\
`python personium.py benzene.png time=5 element=Co c_bg='(0,0,0)' c_orbs='(255,255,255)' c_eborder='(255,255,255)' c_einner='(0,0,0)'`

## Installation

This CLI script is not on pip because it's just a minor editing thingy. Clone the repository and open your favorite terminal in the repository folder. The application is command-line based, but it includes a if `__name__ == 'main'` to import the program as well.

## Dependencies

`numpy`   for everything\
`ast`     for a tuple hack\
`chemlib` for everything concerning chemistry (this module could use an update)\
`PIL`     for image manipulation

## Usage

Syntax (ignore single quotes, not double quotes):
```
python personium.py 'image filepath'
    output='filename; without type'
    filetype='gif or png'
    element='atomic number or symbol'
    time='ℕ; time for the gif'
    mode='cw, ccw, staggered, spin; changes the spinning direction of the atoms
        all clockwise, all ccw, alternating per orbital (obey Aufbau and Hund)
        spin per electron (obeys Aufbau, Hund and Pauli)'
    descriptive='0 or 1; enables descriptive filenames. Ignores output kwarg'
    debug='0 or 1; prints out debugging info to the console'
        c_bg='"(0-255,0-255,0-255)"; RGB tuple for the background color'
        c_orbs='"(0-255,0-255,0-255)"; RGB tuple for the orbital circle color'
        c_eborder='"(0-255,0-255,0-255)"; RGB tuple for the electron border color'
        c_einner='"(0-255,0-255,0-255)"; RGB tuple for the electron inner color'
```

Default:
```
output=atom
filetype=gif
element=randomint(1,118)
time=3
mode=spin
descriptive=0
debug=0
rtuple = f'({randomint(0,255)},{randomint(0,255)},{randomint(0,255)})' #in this case
    c_bg=rtuple
    c_orbs=rtuple
    c_eborder=rtuple
    c_einner=rtuple
```

## Examples

These examples require a file in the same directory called image.png. Open a terminal in the folder personium.py is located in and enter the following:

### Minimal exmaple

In your terminal, the minimal input is:
`python personium.py image.png`\
Outputs atom.gif of 3 s with a random element, mode=spin with all colors random.

### 

`python personium.py image.png output=imageorbitals element=2 mode=staggered time=5 debug=1`\
Outputs imageorbitals.gif of 5 s with Helium orbitals, mode=staggered with all colors random and the console prints debugging info.

###


`python personium.py image.png filetype=png element=Ne descriptive=1 c_bg='(0,0,0)' c_orbs='(255,0,0)' c_eborder='(0,255,0)' c_einner='(0,0,255)'`\
Outputs image-Ne-3s.png with Neon orbitals, mode does not matter, time in filename is a TODO (never) with the background black, the orbitals red, the electron borders green and the inner part of the electrons are red.

## Licence

MIT

## Support

Pull requests are open.

## Why

Intense hatred for front-end development.

## TODO

This script is actually quite fine as it is. I don't think major changes will happen because the script is short and easy to edit (thank god for procedural programming).
- [ ] Perhaps a tool to make the electrons also contain images, that'd be funny I guess
- [ ] More filetypes (APNG, MP4 perhaps)
- [ ] Schrödinger orbitals

### Known bugs

- When rendering a PNG, the descriptive filename still shows the time
- Input parsing is kind of unstable - error messages might not make sense






