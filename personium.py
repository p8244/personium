# Personium
# V1.0
# Crossed_Omega
# 2022-11-27

# This is a silly image manipulation script to draw atomic orbitals around images.



def personium(*args,**kwargs):
    """
    Arguments: Filename. Keywords: filetype (gif,png); element (atom number or symbol); time (gif duration); 
    debug (0 or 1); mode (cw, ccw, staggered, random); descriptive (filename, 0 or 1); 
    colors: c_bg, c_orbs, c_eborder, c_einner (RGB tuples WITH QUOTES)
    """

    ## Libs
    import re
    import numpy as np
    import random as r
    from ast import literal_eval
    from chemlib import Element, PeriodicTable
    from PIL import Image, ImageDraw
    
    
    
    ## imput validation    
    if len(args) != 1:
        raise(Exception('Invalid argument(s) (input file path) (enclose the file path with "..." ?'))
    filepath = args[0]
    
    kwarglist = ['output','filetype','element','time','debug','descriptive','mode','c_bg','c_orbs','c_eborder','c_einner']
    if len(set(kwargs).intersection(set(kwarglist))) != len(kwargs):
        raise(Exception('Illegal keyword detected'))
    
    kwargs = {'output': 'atom','filetype': 'gif', 'element': r.randint(1,118), 'time': 3, 'debug': 0, 'descriptive': 0, 'mode': 'spin', **kwargs}
    
    modelist = ['ccw','cw','staggered','spin']
    if str(kwargs['mode']) not in modelist:
        raise(Exception('Illegal mode detected'))
    
    mode = str(kwargs['mode'])
    
    if kwargs['filetype'] == 'gif' or kwargs['filetype'] == 'png':
        filetype = kwargs['filetype']
    else:
        raise(Exception('Non-supported filetype'))
    
    pte = PeriodicTable()
    try:
        atomnumber = int(kwargs['element'])
        symbol = pte['Symbol'].iloc[atomnumber-1]
    except ValueError:
        symbol = kwargs['element']
        atomnumber = pte.index[pte['Symbol'] == symbol][0]  
        
    element = Element(symbol)
    
    outputname = kwargs['output']
    
    time = int(kwargs['time'])
    
    debug = int(kwargs['debug'])
    
    descriptive = int(kwargs['descriptive'])
    
    def rts():
        return f'({r.randint(0,255)},{r.randint(0,255)},{r.randint(0,255)})'

    #c_dict = {'c_bg': '(255,255,255)', 'c_orbs': '(0,0,0)', 'c_eborder': '(0,0,0)', 'c_einner': '(255,255,255)', **kwargs}
    c_dict = {'c_bg': rts(), 'c_orbs': rts(), 'c_eborder': rts(), 'c_einner': rts(), **kwargs}
    c_dict['c_bg']      = literal_eval(c_dict['c_bg'])
    c_dict['c_orbs']    = literal_eval(c_dict['c_orbs'])
    c_dict['c_eborder'] = literal_eval(c_dict['c_eborder'])
    c_dict['c_einner']  = literal_eval(c_dict['c_einner'])
    
    if debug: print(c_dict);
    
    
    
    ## Image preprocessing
    img = Image.open(filepath)
    
    if debug: print(f'input file: {filepath}');
    
    # Padding image
    imgM = max(img.size)
    imgW,imgH = img.size
    white = Image.new('RGB',(imgM,imgM),(255,255,255))
    white.paste(img,((imgM-imgW)//2,(imgM-imgH)//2))
    img = white
    
    # Mask
    # I stole this code from somewhere as well
    lum_img = Image.new('L', [imgM,imgM] , 0)
    draw = ImageDraw.Draw(lum_img)
    draw.pieslice([(0,0), (imgM,imgM)], 0, 360, fill = 255, outline = "white")
    # Apply mask
    img_arr = np.array(img)
    lum_img_arr = np.array(lum_img)
    final_img_arr = np.dstack((img_arr,lum_img_arr))
    img_v = Image.fromarray(final_img_arr) # Image Vignette
    
    if debug: print(f'inputW: {imgW} px, inputH: {imgH} px, inputMax: {imgM}');


    
    ## Generating orbital info
    # These are vars that are static could be changed later
    spacing = 0.1*imgM # px
    electronsize = 0.05*imgM # px
    rps = 1/time # revs per second, should be integer to loop perfectly # Dont change, this makes sense
    
    conf = element.Config
    while conf[0] == '[':
        elem = Element(conf[1:3])
        conf = elem.Config + conf[4::]

    odict = {}
    for word in conf.split(' '):
        nums = list(map(int, re.findall('\d+', word)))
        if nums[0] not in odict:
            odict[nums[0]] = nums[1]
        else:
            odict[nums[0]] += nums[1]
    
    maxlevel = max(list(odict.keys()))
    
    if debug: print(f'element: {symbol}, e-conf: {odict}, max e-level: {maxlevel}');
    
    # If PNG, just render a single frame
    if filetype == 'png':
        frames = 1
    else:
        frames = int(time * 30)
    angleperframe = ((2*np.pi)/30) * rps # rad / frame
    
    # Canvas
    finalradius = (imgM//2 + maxlevel*spacing + electronsize//2) * 1.1 # Enough space to fit the electrons on the orbitals plus a little more
    finalsize = int(2*finalradius)
    
    if debug: print(f'level spacing: {spacing} px, e-size {electronsize} px, rps: {rps} rad/s, mode: {mode}');
    if debug: print(f'time: {time}, frames: {frames}, angle per frame {angleperframe:.4f} rad/f, final size: {finalsize}x{finalsize} px');
    
    # Electron dict
    # Lmao all this just to obey Aufbau, Hund and Pauli
    spins = '01' + '01000111'*2 + '010000011111000111'*2 + '01000000011111110000011111000111'*2
    
    anglemode = lambda x: x[1]*(2*np.pi/x[0]) - np.pi/2
    if mode == 'ccw':
        spinmode  = lambda x: 1
    if mode == 'cw':
        spinmode  = lambda x: -1
    if mode == 'staggered':
        spinmode  = lambda x: (x[0]%2)*2-1
    if mode == 'spin':
        spinmode  = lambda x: (int(spins[x[1]-1]))*2-1
        roffset = 2*np.pi*r.random()
        anglemode = lambda x: x[1]*(2*np.pi/x[0]) - (np.pi/2) - roffset*x[0]
    
    A = 1
    electrons = []
    for o in odict: #orbital, electron, spin, radius, angle, wspeed
        for e in range(odict[o]):
            electrons.append({'o': o, 'e': e+1, 's': spinmode((o,A)), 'r': imgM/2 + spacing*o, 'a': anglemode((odict[o],e)), 'w': angleperframe})
            A += 1
            
            
    
    ## Generate the final image
    # Canvas setup
    finalimg = Image.new('RGB', [finalsize,finalsize], color = c_dict['c_bg'])
    offset = ((finalsize - imgM) // 2, (finalsize - imgM) // 2)
    finalimg.paste(img_v, offset, mask = img_v.getchannel('A'))
    draw = ImageDraw.Draw(finalimg)
    
    # Orbital circles
    for level in range(1,maxlevel+1):
        tl = finalsize/2 - imgM/2 - spacing*level
        br = finalsize/2 + imgM/2 + spacing*level
        draw.ellipse((tl, tl, br, br), outline = c_dict['c_orbs'], width = 3)
        
        if debug: print(f'orbital {level} tl,br: {(tl,br)}');
    
    # Generate frames
    finalimgEmpty = finalimg.copy()
    framearray = []
    for frame in range(frames):
        finalimg = finalimgEmpty.copy()
        draw = ImageDraw.Draw(finalimg)
        for e in electrons:
            e_mp_x = finalsize/2 + e['r']*np.cos(e['a']) # electron midpoint x
            e_mp_y = finalsize/2 + e['r']*np.sin(e['a']) # electron midpoint y
            e_halfsize = electronsize//2
            e_bb_tlx = e_mp_x-e_halfsize # electron bounding box topleft x
            e_bb_tly = e_mp_y-e_halfsize # electron bounding box topleft y
            e_bb_brx = e_mp_x+e_halfsize # electron bounding box botright x
            e_bb_bry = e_mp_y+e_halfsize # electron bounding box botright y
            draw.ellipse((e_bb_tlx, e_bb_tly, e_bb_brx, e_bb_bry), outline = c_dict['c_eborder'], width = 3, fill = c_dict['c_einner']) # can be floats huh
            e['a'] -= e['w']*e['s']
        framearray.append(finalimg)
    
    if descriptive:
        outputfilename = f'{filepath[0:-4]}-{symbol}-{time}s.{filetype}'
    else:
        outputfilename = f'{outputname}.{filetype}'
        
    frame_one = framearray[0]
    frame_one.save(outputfilename, format=filetype, append_images=framearray, save_all=True, duration=int(1000/30), loop=0)
    # comment = '↑↑↓↓←→←→BA' doesn't work :(
    
    if debug: print(f'output: {outputfilename}\nsuccess\n')


    
    



    
if __name__ == '__main__':
    # Thanks https://stackoverflow.com/a/56345261
    import sys
    argv=sys.argv[1:] 
    kwargs={kw[0]:kw[1] for kw in [ar.split('=') for ar in argv if ar.find('=')>0]}
    args=[arg for arg in argv if arg.find('=')<0]
    personium(*args,**kwargs)